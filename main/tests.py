from django.test import LiveServerTestCase, TestCase, Client, tag
from django.urls import resolve, reverse
from selenium import webdriver
from . import views

class Teststory8(TestCase):
    def test_url_main(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_cek_hasil_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_function_views_used(self):
        function = resolve('/')
        self.assertEqual(function.func, views.home)
